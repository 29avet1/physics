'use strict';

/* Controllers */

var physicsControllers = angular.module('physicsControllers', []);

physicsControllers.controller('HeaderController', [
    '$scope',
    '$routeParams',
    '$location',
    '$window',
    'categories',
    function ($scope,$routeParams, $location,$window,categories) {

        $scope.isActive = function (viewLocation) {
            return $location.path().indexOf(viewLocation) !== -1;
        };
        $scope.subIsActive = function(category_id){
            return category_id == $routeParams.category_id;
        };
        categories.success(function(data){
            var categories = [];
            data.map(function(item) {
                categories[item.type_id] = item.name;
            });
            $scope.categories = data;
            $scope.categorynames = categories;
        });

        footer_to_bottom();

        angular.element($window).bind('resize', function() {
            footer_to_bottom();
        });

        function footer_to_bottom() {
            var top = $(window).innerHeight() - $('footer').outerHeight();

            var position = $('footer').position();
            var scondsryContentMinHeight = $('.page_content').outerHeight() + top - position.top -90;
            if (top > position.top) {
                $('.page_content').css({
                    "min-height": top
                });
            } else {
                $('.page_content').css({
                    "min-height": 'auto'
                });
            }
        }
    }
]);

physicsControllers.controller('homePageController', [
    '$scope',
    'renderInfo',
    'topArticles',
    function($scope,renderInfo,topArticles) {

        topArticles.success(function(data){
            $scope.topArticles = data;
        });

        renderInfo.success(function(data){
            $scope.posts = data;
        });
    }
]);


physicsControllers.controller('ArticlesController', [
    '$scope',
    '$rootScope',
    '$routeParams',
    '$location',
    'allArticles',
    'articlesByCategory',
    'articlesCount',
    function($scope,$rootScope,$routeParams,$location,allArticles,articlesByCategory,articlesCount) {

        $scope.category_id = false;

        if($location.path().indexOf('byCategory') === -1){
            allArticles.getAll($routeParams.page).success(function(data){
                $scope.allArticles = data;
                $scope.paginationLink = '/articles/';
            });
        }else{
            articlesByCategory.getAll($routeParams.category_id ,$routeParams.page).success(function(data){
                $scope.allArticles = data;
                $scope.category_id = $routeParams.category_id;
                $scope.paginationLink = '/articles/byCategory/' + $routeParams.category_id + '/';
            });
        }

        articlesCount.get($routeParams.category_id).success(function (data) {
            $scope.linksCount = Math.ceil(data / $rootScope.articleLimit);
        });
    }
]);

physicsControllers.controller('ArticleController', [
    '$scope',
    '$routeParams',
    'articleData',
    function($scope,$routeParams,articleData) {
        articleData.get($routeParams.article_id).success(function (data) {
            $scope.article = data;
        });

    }
]);

physicsControllers.controller('ContactController', [
    '$scope',
    'contactData',
    function($scope,contactData) {
        contactData.success(function (data) {
            $scope.contact = data;
        });
    }
]);
