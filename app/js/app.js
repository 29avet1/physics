'use strict';

/* App Module */
// console.log('%cSTAFF ONLY!!!',"color:red;font-size:50px");

var physicsApp = angular.module('physicsApp', [
  'ngRoute',
  'ngSanitize',
  'physicsControllers'
]).run(function($rootScope) {

  $rootScope.apiUrl = "http://api-physics.dev";
  // $rootScope.apiUrl = "http://192.168.6.84:9999";
  $rootScope.articleLimit = 9;
  $rootScope.timeOut = 200;
});


physicsApp.config([
  '$routeProvider',
  '$locationProvider',
  function($routeProvider,$locationProvider) {
    $routeProvider.
      when('/', {
        redirectTo: '/home'
      })
      .when('/home', {
        templateUrl: 'html/views/homepage.html',
        controller: 'homePageController',
      })
      .when('/articles/:page?', {
        templateUrl: 'html/views/articles.html',
        controller: 'ArticlesController',
      })
      .when('/articles/byCategory/:category_id/:page?', {
        templateUrl: 'html/views/articles.html',
        controller: 'ArticlesController',
      })
      .when('/article/:article_id', {
        templateUrl: 'html/views/article.html',
        controller: 'ArticleController',
      })
      .when('/contact', {
        templateUrl: 'html/views/contact.html',
        controller: 'ContactController',
      })
      .when('/err-404', {
        templateUrl: 'html/views/404.html',
      })
      .otherwise({
        redirectTo: '/err-404'
      });

      $locationProvider.html5Mode(true);

  }
]);
