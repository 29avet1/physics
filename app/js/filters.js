'use strict';

/* Filters */

physicsApp.filter('range', function() {
  return function(input, total) {
    total = parseInt(total);

    for (var i=1; i<=total; i++) {
      input.push(i);
    }

    return input;
  };
});

physicsApp.filter('trusted', function($sce){
  return function(html){
    return $sce.trustAsHtml(html);
  }
});
