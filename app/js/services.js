'use strict';

/* Services */

    physicsApp.factory('categories', [
        '$http',
        '$rootScope',
        function($http,$rootScope) {

            // return all categories
            return $http.get($rootScope.apiUrl+'/articles/categories')
                .success(function(data) {
                    return data;
                })
                .error(function(err) {
                    return err;
                });
        }
    ]);


    physicsApp.factory('renderInfo', [
        '$http',
        '$rootScope',
        function($http,$rootScope) {
            return $http.get($rootScope.apiUrl+'/articles/allByCategory/4')
                .success(function(data) {
                    return data;
                })
                .error(function(err) {
                    return err;
                });
        }
    ]);

    physicsApp.factory('articleData', [
            '$http',
            '$rootScope',
            function($http,$rootScope) {
                return {
                    get: function(article_id){
                        var url = $rootScope.apiUrl+'/articles/post/'+article_id;
                        return $http.get(url)
                            .success(function(data) {
                                return data;
                            })
                            .error(function(err) {
                                return err;
                            });
                    }
                }
            }
    ]);

    physicsApp.factory('articlesCount', [
        '$http',
        '$rootScope',
        function($http,$rootScope) {
            //return posts by category  /byCategory/{category_id}/{limit?}/{offset?}

            return {
                get: function(category_id){
                    var url = $rootScope.apiUrl+'/articles/articlesCount/';
                    if(typeof category_id != 'undefined'){
                        url += category_id;
                    }
                    return $http.get(url)
                        .success(function(data) {
                            return data;
                        })
                        .error(function(err) {
                            return err;
                        });
                }
            }

        }
    ]);

    physicsApp.factory('allArticles', [
        '$http',
        '$rootScope',
        function($http,$rootScope) {
        // return posts by limit  /articles/allArticles/{limit?}/{offset?}
            return{
                getAll: function(offset){
                    if( typeof offset == 'undefined'){
                        offset = 1;
                    }
                    return $http.get($rootScope.apiUrl+'/articles/allArticles/' + $rootScope.articleLimit + '/'+offset)
                        .success(function(data) {
                            return data;
                        })
                        .error(function(err) {
                            return err;
                        });
                }
            }
        }
    ]);

    physicsApp.factory('articlesByCategory', [
        '$http',
        '$rootScope',
        function($http,$rootScope) {
        // return posts by limit  /articles/allArticles/{limit?}/{offset?}
            return{
                getAll: function(category_id,offset){
                    if( typeof offset == 'undefined'){
                        offset = 1;
                    }
                    return $http.get($rootScope.apiUrl+'/articles/byCategory/'+ category_id + '/'+ $rootScope.articleLimit + '/'+offset)
                        .success(function(data) {
                            return data;
                        })
                        .error(function(err) {
                            return err;
                        });
                }
            }
        }
    ]);

    physicsApp.factory('topArticles', [
        '$http',
        '$rootScope',
        function($http,$rootScope) {
            return $http.get($rootScope.apiUrl+'/articles/topArticles')
                .success(function(data) {
                    return data;
                })
                .error(function(err) {
                    return err;
                });
        }
    ]);

physicsApp.factory('contactData', [
        '$http',
        '$rootScope',
        function($http,$rootScope) {
            return $http.get($rootScope.apiUrl+'/contacts')
                .success(function(data) {
                    return data;
                })
                .error(function(err) {
                    return err;
                });
        }
    ]);