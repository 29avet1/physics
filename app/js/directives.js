'use strict';



physicsApp.directive('shortInfo', function() {
    return {
        restrict: 'E',
        transclude : false,
        scope: {
            post: '=',
            size: '='
        },
        templateUrl: 'html/directives/shortInfo.html'
    };
});

physicsApp.directive('toTop', function() {
    return {
        restrict: 'E',
        transclude : false,
        templateUrl: 'html/directives/toTopArrow.html',
        link: function ($scope, $elem, $attrs) {
            window.addEventListener('scroll',function(){
                if($(window).scrollTop()>600) {
                    $elem.show(0);
                }else{
                    $elem.hide(0);
                };

                if($(window).scrollTop()>5){
                    $elem.addClass('nav_medium');
                }else{
                    $elem.removeClass('nav_medium');
                }

            });
            //----------------------Scroll to top-------------------------//
            $elem.mousedown(function(){
                $(this).addClass('pushed');
            });
            $elem.mouseup(function(){
                $(this).removeClass('pushed');
                $("html,body").animate({scrollTop:0}, 500);
            });
            //-----------------------------------------------------------//
        }
    };
});

physicsApp.directive('pagination',[
    '$routeParams',
    '$rootScope',
    function($routeParams,$rootScope) {
    return {
        restrict: 'E',
        transclude : false,
        scope: {
            count: '=',
            link: '='
        },
        templateUrl: 'html/directives/pagination.html',
        link: function($scope, $elem) {
            setTimeout(function () {
                var step = 0;
                var activePage = 1;

                if(typeof $routeParams.page != 'undefined'){
                   activePage = $routeParams.page;
                };

                $elem.find('.link_no_' + activePage).addClass('active');

                if($scope.count == 1){
                    $elem.hide();
                    return true;
                }else if($scope.count <= 5){
                    $elem.find('.prev, .next').addClass('invisible');
                    step = (5 - $scope.count) * 16;
                    // return true;
                }else{
                    console.log($scope.count);
                    step = $elem.find('.page_link.active').position().left - 58;
                }

                paging($elem,step);

                $elem.find('.prev').on('click', function() {
                    if(!$(this).hasClass('inactive')){
                        step -= 145;
                        paging($elem,step);
                    };
                });
                $elem.find('.next').not('.inactive').on('click', function() {
                    if(!$(this).hasClass('inactive')){
                        step += 145;
                        paging($elem,step);
                    };
                });
            },$rootScope.timeOut);

            function paging($elem,step) {
                if($elem.find('.slide-list').width() < 145){
                    $elem.find('.slide-list').css({
                        left: step
                    });
                    return true;
                }
                if(step <= 0){
                    step = 0;
                    $elem.find('.prev').addClass('inactive');
                }else{
                    $elem.find('.prev').removeClass('inactive');
                }

                if((step >= $elem.find('.slide-list').width() - 145)){
                    step = $elem.find('.slide-list').width() - 145;
                    $elem.find('.next').addClass('inactive');
                }else{
                    $elem.find('.next').removeClass('inactive');
                }

                $elem.find('.slide-list').css({
                    left: -step
                });
            }
        }
    }
}]);


physicsApp.directive('solarSystem',[
    '$window',
    '$location',
    '$rootScope',
    function ($window,$location,$rootScope) {
    return {
        restrict: 'E',
        templateUrl: 'html/directives/solarSystem.html',
        link: function ($scope, $elem, $attrs) {
            angular.element($window).bind('load', function () {
                setTimeout(function () {
                    loadSolarSystem();
                },$rootScope.timeOut + 50);
            });

            planetsWisibility();

            $scope.$on('$routeChangeStart', function(next, current) {
                planetsWisibility();
                setTimeout(function () {
                    loadSolarSystem();
                },$rootScope.timeOut);
            });

            function planetsWisibility() {
                if($location.path().indexOf('/err-404') !== -1){
                    $('.planet').hide();
                }else{
                    $('.planet').show();
                }
            }

            function loadSolarSystem(){
                var jarallax = new Jarallax();
                jarallax.addAnimation('.mercury', [{progress: '0%', top: '0%'}, {progress: '100%', top: '-10%'}]);
                jarallax.addAnimation('.venus', [{progress: '0%', top: '0%'}, {progress: '100%', top: '35%'}]);
                jarallax.addAnimation('.earth', [{progress: '0%', top: '35%'}, {progress: '100%', top: '-90%'}]);
                jarallax.addAnimation('.mars', [{progress: '0%', top: '40%'}, {progress: '100%', top: '-70%'}]);
                jarallax.addAnimation('.saturn', [{progress: '0%', top: '100%'}, {progress: '100%', top: '-100%'}]);
                jarallax.addAnimation('.jupiter', [{progress: '0%', top: '140%'}, {progress: '100%', top: '-150%'}]);
                jarallax.addAnimation('.uranus', [{progress: '0%', top: '180%'}, {progress: '100%', top: '-170%'}]);
                jarallax.addAnimation('.neptune', [{progress: '0%', top: '210%'}, {progress: '100%', top: '-300%'}]);
                jarallax.addAnimation('.pluto', [{progress: '0%', top: '190%'}, {progress: '100%', top: '-280%'}]);

                jarallax.addAnimation('.stars', [{progress: '0%', top: '0px'}, {progress: '100%', top: '50px'}]);
                jarallax.setProgress(0.0001);
            };
        }
    };

}]);

physicsApp.directive('navigation', function () {
    return {
        restrict: 'E',
        templateUrl: 'html/directives/navigation.html',
        link: function () {

            //_____________________________mobile menu ________________________________//
            mobile_menu();
            var mobile_height=window.innerHeight - 100;
            $('.mobile_menu').css('height',mobile_height);

            $(window).resize(function(){
                mobile_menu();
                mobile_height=window.innerHeight - 100;
                $('.mobile_menu').css('height',mobile_height);
            });

            $(".mobile_open_but").click(function(){
                $('.mobile_menu_cont').toggleClass('left0');
                $('.cover').toggle();
                var child = $('.material-design-hamburger__layer');
                if (child.hasClass('material-design-hamburger__icon--to-arrow')) {
                    child.removeClass('material-design-hamburger__icon--to-arrow');
                    child.addClass('material-design-hamburger__icon--from-arrow');
                } else {
                    child.removeClass('material-design-hamburger__icon--from-arrow');
                    child.addClass('material-design-hamburger__icon--to-arrow');
                }

            });

            $('.mobile_menu a').click(function(){
                $('.mobile_menu_cont').removeClass('left0');
                $('.cover').hide();
                var child = $('.material-design-hamburger__layer');
                child.removeClass('material-design-hamburger__icon--to-arrow');
                child.addClass('material-design-hamburger__icon--from-arrow');
            })

            $(document).click(function(event){
                if(!$(event.target).closest('.mobile_menu_cont').length){
                    $('.mobile_menu_cont').removeClass('left0');
                    $('.cover').hide();
                    var child = $('.material-design-hamburger__layer');
                    child.removeClass('material-design-hamburger__icon--to-arrow');
                    child.addClass('material-design-hamburger__icon--from-arrow');
                }
            });
            //_____________________________________________________________________________//

            //__________________________Submenu open_____________________________________//
            $('.sub_open_button').click(function(){
                var arrow = $(this).children('.sub_open_arrow');
                if($(arrow).hasClass('sub_left_arrow')){
                    $(arrow).replaceWith('<span class="sub_open_arrow sub_top_arrow"></span>');
                }else{
                    $(arrow).replaceWith('<span class="sub_open_arrow sub_left_arrow"></span>');
                }
                $('.mob_with_sub').toggleClass('hidden');

            });
            //____________________________________________________________________________//


            function mobile_menu(){
                $('.material-design-hamburger__layer')
                    .removeClass('material-design-hamburger__icon--to-arrow')
                    .addClass('material-design-hamburger__icon--from-arrow');
            }
        }
    }
});


physicsApp.directive('slick',[
    '$window',
    '$rootScope',
    function($window,$rootScope) {
    return {
        restrict: 'E',
        transclude : false,
        scope: {
            toparticles: '='
        },
        templateUrl: 'html/directives/slick.html',
        link: function($scope, $elem){
            $scope.onResize = function() {
                setTimeout(function () {
                    $elem.slick({
                        dots: true,
                        infinite: true,
                        autoplay: true,
                        autoplaySpeed: 5000,
                        mobileFirst: true,
                    });
                    sliderMaxHeight();
                },$rootScope.timeOut);
            };
            $scope.onResize();

            angular.element($window).bind('resize', function() {
                sliderMaxHeight();
            });

            function sliderMaxHeight(){
                var heights = $(".my-slick-item img").map(function ()
                    {
                        return $(this).innerHeight();
                    }).get(),

                    maxHeight = Math.min.apply(null, heights);

                $('#my-slick,.my-slick-item').css({
                    "height" : maxHeight - 10
                });
            }
        }
    };
}]);

//
// physicsApp.directive('windowResizeFunctions',['$window','$timeout', function ($window,$timeout) {
//
//     return {
//         link: function($scope) {
//             $scope.onResize = function() {
//                 $timeout(function () {
//                     loadSolarSystem();
//                 }, 1000);
//             };
//             $scope.onResize();
//
//             angular.element($window).bind('resize', function() {
//                 loadSolarSystem();
//             });
//         }
//     };
// }]);